const express = require('express');
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');
const app = express();
const port = 3001;

//Mongodb atlas
//Connect to the database by passing in your connection string, remember to replace <password> and database name with actual values.
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z7wiz.mongodb.net/Batch127_to-do?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
);
//Connecting to Robo3t locally
/*
mongoose.connect("mongodb://localhost:27017/database name", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
*/
//set notifications for connection success or failure
//Connection to the database, allows us to handle errors when the initial connection is established 
let db = mongoose.connection;
//If a connection error occured, output in the console
//console.error.bind(console) = allows us to print error in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));
//if the connection is successful, output in the console
db.once("open", ()=> console.log("We're connected to the cloud database"))

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

//Mongoose Schemas

//Schemas determine the structure of the documents to be written in the database.
//Schemas act as blueprints to our data
//Use the Schema() constructor of the Mongoose module to create a new Schema object
//The "new" keyword create a new Schema
const taskSchema = new mongoose.Schema({
	//Define the fields with the corressponding data type
	//for task, it needs a "task name" and "task status"
	name: String,
	status: {
		type: String,
		//Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
})
//Task is capitalized following the MVC approach for naming convention
//Model-View-Controller
const Task = mongoose.model("Task", taskSchema)
//The first parameter of the Mongoose model method indicates the collection in where to store the data
//The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection



//Create a new task
/*
Business Logic
1. Add a functionality to check if there are duplicate tasks
	-if the task already exists in the database, we return an error
	-if the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema default is to "pending" upon creation of an object
*/
app.post('/tasks', (req, res)=>{
	//check if there are duplicate tasks
	//findOne is a mongoose method that acts similar to "find" of mongoDB
	//findOne() returns the first document that matches the search criteria
	Task.findOne({ name: req.body.name }, (err, result)=>{
		//if a document was found and the document's name matches the information sent via the client/postman
		if(result !== null && result.name == req.body.name ){
			//return a message to the client
			return res.send("Duplicate task found")
		}else{
			//if no document was found, create a new task and save it to our database
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("New task created")
				}
			})
		}

	})
})


//Getting all the tasks 

/*
Business Logic
1. We will retrive all the documents using the get method
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/postman and return an array of document
*/

app.get('/tasks', (req, res)=>{
	//"find" is a mongoose method that is similar to MongoDB "find". empty {} means it returns ALL the documents and stores them in the result parameter of the callback function
	Task.find({}, (err, result)=>{
		//if an error occured
		if(err){
			return console.log(err)
		}else{
			//if no errors are found
			//status 200 means that everything is OK in terms of processing
			//the "json" method allows to send a JSON format for the response
			//The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				data: result
			})
		}
	})
})
/*
Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.

*/
/*
Business Logic for POST /signup
1. Add a functionality to check if there are duplicate tasks
	-If the user already exists in the database, we return an error
	-If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new user object with a "username" and "password" fields/properties

*/

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema);

//3. Create a POST route that will access the "/signup" route that will create a user.
//Answer:
app.post('/signup', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) => {
		if(result !== null && result.username == req.body.username ){
			return res.send("User are already exist")
		}else{
			let newUser = new User({
				username: req.body.username
			})

			newUser.save((saveErr, savedSuc)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("Succesfully Created")
				}
			})
		}
	})
})

//5. Create a GET route that will return all users.
app.get('/users', (req, res)=>{
	User.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})





app.listen(port , () => console.log(`Server is running at port ${port}`));
